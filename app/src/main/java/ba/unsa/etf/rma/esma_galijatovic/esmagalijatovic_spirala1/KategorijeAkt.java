package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity {
    public static ArrayList<String> kategorije;
    public static ArrayList<Knjiga> upisaneKnjige;
    public static ArrayList<Autor> autori;
    public static ArrayList<String> autoriString;
    public static Boolean siriL= false;
    public static BazaOpenHelper openHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        kategorije= new ArrayList<String>();
        upisaneKnjige= new ArrayList<Knjiga>();
        autori = new ArrayList<>();
        autoriString = new ArrayList<>();
        openHelper = new BazaOpenHelper(getBaseContext());
        kategorije = openHelper.dajKategorije();
        autoriString = openHelper.dajAutore();
        upisaneKnjige = openHelper.dajKnjige();
        if(kategorije.isEmpty()){
            openHelper.dodajKategoriju("test1");
            openHelper.dodajKategoriju("test2");
            kategorije = openHelper.dajKategorije();

        }

        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_kategorije_akt);
        FragmentManager fm = getFragmentManager();
        FrameLayout ldetalji =(FrameLayout)findViewById(R.id.mjestoF2);
        FrameLayout lcitav =(FrameLayout)findViewById(R.id.mjestoF3);
        if(lcitav!=null){
            lcitav.setVisibility(View.INVISIBLE);
        }
        if(ldetalji!=null){
            siriL=true;
            Button dKategorije = (Button) findViewById(R.id.dKategorije);
            Button dAutori = (Button) findViewById(R.id.dAutori);
            dKategorije.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    KnjigeFragment fd;
                    FragmentManager fm = getFragmentManager();

                        fd = new KnjigeFragment();
                        if (!kategorije.isEmpty()) {
                            Bundle argumenti = new Bundle();
                            argumenti.putString("zanr", kategorije.get(0));
                            fd.setArguments(argumenti);
                        }
                        fm.beginTransaction().replace(R.id.mjestoF2, fd).commit();

                    ListeFragment f1 = null;//(ListeFragment) fm.findFragmentByTag("Lista1");
                    if (f1 == null) {
                        f1 = new ListeFragment();
                    }
                    Bundle argumenti = new Bundle();
                    argumenti.putStringArrayList("lista_kategorija", kategorije);
                    f1.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoF1, f1, "Lista1").addToBackStack(null).commit();
                    // za siroke ekrane
                }});

            dAutori.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    KnjigeFragment fd;
                    FragmentManager fm = getFragmentManager();


                        fd = new KnjigeFragment();
                        if (!autoriString.isEmpty()) {
                            Bundle argumenti = new Bundle();
                            argumenti.putString("autor", autoriString.get(0));
                            fd.setArguments(argumenti);
                        }
                        fm.beginTransaction().replace(R.id.mjestoF2, fd).commit();

                    ListeFragment f1 = null;//(ListeFragment) fm.findFragmentByTag("Lista1");
                    if (f1 == null) {
                        f1 = new ListeFragment();
                    }
                    Bundle argumenti = new Bundle();
                    argumenti.putStringArrayList("lista_autora", autoriString);
                    f1.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoF1, f1, "Lista1").addToBackStack(null).commit();
                    // za siroke ekrane
                }});
        }
        else {
            siriL=false;
            final Button dKategorije = (Button) findViewById(R.id.dKategorije);
            final Button dAutori = (Button) findViewById(R.id.dAutori);
            final Button dOnline = (Button) findViewById(R.id.dDodajOnline);

            dKategorije.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm = getFragmentManager();
                    ListeFragment f1;// = (ListeFragment) fm.findFragmentByTag("Lista");

                    f1 = new ListeFragment();
                    Bundle argumenti = new Bundle();
                    argumenti.putStringArrayList("lista_kategorija", kategorije);
                    f1.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjesto1, f1, "Lista").addToBackStack(null).commit();

                }
            });

            dAutori.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm = getFragmentManager();
                    ListeFragment f1 ;//= (ListeFragment) fm.findFragmentByTag("Lista");//-------------------------------------
                    //  if(f1==null){
                    f1 = new ListeFragment();
                    Bundle argumenti = new Bundle();
                    argumenti.putStringArrayList("lista_autora",autoriString);
                    f1.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjesto1, f1, "Lista").addToBackStack(null).commit();

                    // }
                }
            });

            dOnline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FrameLayout f1=(FrameLayout)findViewById(R.id.mjesto1);
                    f1.setVisibility(View.INVISIBLE);
                    dAutori.setVisibility(View.INVISIBLE);
                    dKategorije.setVisibility(View.INVISIBLE);
                    dOnline.setVisibility(View.INVISIBLE);
                    FrameLayout online = (FrameLayout)findViewById(R.id.mjestoOnline);
                    online.setVisibility(View.VISIBLE);
                    FragmentManager fm = getFragmentManager();
                    FragmentOnline fo ;//= (ListeFragment) fm.findFragmentByTag("Lista");//-------------------------------------
                    //  if(f1==null){
                    fo = new FragmentOnline();
                    fm.beginTransaction().replace(R.id.mjestoOnline, fo, "Online").addToBackStack(null).commit();

                }
            });

        }


        /* final ListView lista =(ListView)findViewById(R.id.listaKategorija);

        adapter = new ArrayAdapter <String>(this,android.R.layout.simple_list_item_1, kategorije);
        lista.setAdapter(adapter);

        final Button dugme = (Button)findViewById(R.id.dDodajKategoriju);
        dugme.setEnabled(false);


        // dugme dodaj kategoriju ako je lista prazna nakon pretrage
        dugme.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText unos = (EditText)findViewById(R.id.tekstPretraga);
                if(!unos.getText().toString().isEmpty()) {
                    kategorije.add(0, unos.getText().toString());
                    dugme.setEnabled(false);
                    adapter= new ArrayAdapter<String>(KategorijeAkt.this, android.R.layout.simple_list_item_1,kategorije);
                    lista.setAdapter(adapter);
                }
            }
        });

        Button pretraga= (Button)findViewById(R.id.dPretraga);

        // dugme pretraga
        pretraga.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText tekst=(EditText)findViewById(R.id.tekstPretraga);
                ArrayList<String> rezultatPretrage= new ArrayList<String>();

               // if( !tekst.getText().toString().isEmpty()) {
                    (KategorijeAkt.this).adapter.getFilter().filter(tekst.getText().toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if (count == 0) {
                                dugme.setEnabled(true);
                            }
                        }
                    });
                //}

            }
        });



        Button dugmeDodajKnjigu = (Button)findViewById(R.id.dDodajKnjigu);
        // dodaj knjigu klik
        dugmeDodajKnjigu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i= new Intent(KategorijeAkt.this, DodavanjeKnjigeAkt.class);
                startActivity(i);
            }
        });


        // otvaranje liste knjiga klikom na zanr
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                Intent intent = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                intent.putExtra("zanr", kategorije.get(position));
                startActivity(intent);
            }
        });*/


    }

    public static boolean imaLiAutora(String ime){
        for (String a :autoriString
             ) {
            if(a.equals(ime)) {
                return true;
            }
        }
        return false;
    }

    public static boolean imaLiKnjige(String ID){
        for (Knjiga k:upisaneKnjige
             ) {
            if(k.getId().equals(ID)) return true;
        }
        return false;
    }

    public static boolean imaLiKategorije(String y){
        for (String x: kategorije
             ) {
            if(x==y) return true;
        }
        return false;
    }

}
