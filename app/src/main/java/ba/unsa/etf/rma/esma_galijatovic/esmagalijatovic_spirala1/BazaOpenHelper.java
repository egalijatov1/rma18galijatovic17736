package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by USAID on 27-May-18.
 */

public class BazaOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "baza.db";
    public static final String DATABASE_TABLE_KAT = "Kategorija";
    public static final String DATABASE_TABLE_KNJ = "Knjiga";
    public static final String DATABASE_TABLE_AUTOR = "Autor";
    public static final String DATABASE_TABLE_AUTORSTVO ="Autorstvo";
    public static final int DATABASE_VERSION = 1;
    public static final String KAT_ID = "_id";
    public static final String KAT_NAZIV ="naziv";
    public static final String KNJ_ID = "_id";
    public static final String KNJ_NAZIV = "naziv";
    public static final String KNJ_OPIS = "opis";
    public static final String KNJ_DATUM = "datumObjavljivanja";
    public static final String KNJ_BROJ_STR = "brojStranica";
    public static final String KNJ_ID_WEB = "idWebServis";
    public static final String KNJ_ID_KAT = "idkategorije";
    public static final String KNJ_SLIKA = "slika";
    public static final String KNJ_PREGLEDANA = "pregledana";
    public static final String AUTOR_ID ="_id";
    public static final String AUTOR_IME ="ime";
    public static final String AUTORSTVO_ID ="_id";
    public static final String AUTORSTVO_AUTOR = "idautora";
    public static final String AUTORSTVO_KNJIGA = "idknjige";

    private static final String DATABASE_CREATE_KAT = "create table " + DATABASE_TABLE_KAT + " (" + KAT_ID + " integer primary key autoincrement, " + KAT_NAZIV + " text not null);";
    private static final String DATABASE_CREATE_KNJ = "create table " + DATABASE_TABLE_KNJ + " (" + KNJ_ID + " integer primary key autoincrement, " + KNJ_NAZIV + " text not null, " + KNJ_OPIS + " text, " + KNJ_DATUM + " text, " + KNJ_BROJ_STR + " integer, " + KNJ_ID_WEB + " text not null, " + KNJ_ID_KAT + " integer, " + KNJ_SLIKA + " text, " + KNJ_PREGLEDANA + " integer);";
    private static final String DATABASE_CREATE_AUTOR = "create table " + DATABASE_TABLE_AUTOR + " (" + AUTOR_ID + " integer primary key autoincrement, " + AUTOR_IME + " text not null);";
    private static final String DATABASE_CREATE_AUTORSTVO = "create table " + DATABASE_TABLE_AUTORSTVO + " (" + AUTORSTVO_ID + " integer primary key autoincrement, " + AUTORSTVO_AUTOR + " integer not null, " + AUTORSTVO_KNJIGA + " integer);";

    public BazaOpenHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(DATABASE_CREATE_KNJ);
        db.execSQL(DATABASE_CREATE_KAT);
        db.execSQL(DATABASE_CREATE_AUTOR);
        db.execSQL(DATABASE_CREATE_AUTORSTVO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KAT);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KNJ);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTORSTVO);

        onCreate(db);
    }

    String[] koloneKnjige = new String[]{BazaOpenHelper.KNJ_ID, BazaOpenHelper.KNJ_NAZIV, BazaOpenHelper.KNJ_OPIS, BazaOpenHelper.KNJ_DATUM, BazaOpenHelper.KNJ_BROJ_STR, BazaOpenHelper.KNJ_ID_WEB, BazaOpenHelper.KNJ_ID_KAT, KNJ_SLIKA, KNJ_PREGLEDANA};
    String[] koloneAutorstvo = new String[]{BazaOpenHelper.AUTORSTVO_ID, BazaOpenHelper.AUTORSTVO_AUTOR, BazaOpenHelper.AUTORSTVO_KNJIGA};
    String[] koloneAutor = new String[]{BazaOpenHelper.AUTOR_ID, BazaOpenHelper.AUTOR_IME};
    String[] koloneKateg = new String[]{BazaOpenHelper.KAT_ID, BazaOpenHelper.KAT_NAZIV};
    SQLiteDatabase db;

    public ArrayList<String> dajKategorije() {
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            db=this.getReadableDatabase();
        }
        ArrayList<String> kat = new ArrayList<>();
        Cursor c = db.query(BazaOpenHelper.DATABASE_TABLE_KAT, koloneKateg, null, null, null, null, null);
        int INDEX = c.getColumnIndex(BazaOpenHelper.KAT_NAZIV);
        while (c.moveToNext()) {
            kat.add(c.getString(INDEX));
        }
        c.close();
        db.close();
        return kat;
    }

    public ArrayList<Knjiga> dajKnjige() {
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            db=this.getReadableDatabase();
        }
        ArrayList<Knjiga> knjige = new ArrayList<>();

        Cursor c = db.query(BazaOpenHelper.DATABASE_TABLE_KNJ, koloneKnjige, null, null, null, null, null);
        int Iid = c.getColumnIndex(BazaOpenHelper.KNJ_ID);
        int Inaziv = c.getColumnIndex(BazaOpenHelper.KNJ_NAZIV);
        int Iopis = c.getColumnIndex(BazaOpenHelper.KNJ_OPIS);
        int Idatum = c.getColumnIndex(BazaOpenHelper.KNJ_DATUM);
        int IbrStr = c.getColumnIndex(BazaOpenHelper.KNJ_BROJ_STR);
        int IidWeb = c.getColumnIndex(BazaOpenHelper.KNJ_ID_WEB);
        int ISlika = c.getColumnIndex(KNJ_SLIKA);
        int Ipreg = c.getColumnIndex(KNJ_PREGLEDANA);
        while (c.moveToNext()) {
            ArrayList<Autor> autori = new ArrayList<>();
            int id = c.getInt(Iid);
            String naziv = c.getString(Inaziv);
            String opis = c.getString(Iopis);
            String datum = c.getString(Idatum);
            String idWeb = c.getString(IidWeb);
            String slika = c.getString(ISlika);
            int pregledana = c.getInt(Ipreg);
            int brStr = c.getInt(IbrStr);
            Cursor c1 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneAutorstvo, BazaOpenHelper.AUTORSTVO_KNJIGA + "='" + Integer.toString(id)+"'", null, null, null, null);
            int autor = c1.getColumnIndex(BazaOpenHelper.AUTORSTVO_AUTOR);
            while (c1.moveToNext()) {
                int autorID = c1.getInt(autor);
                Cursor c2 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, koloneAutor, BazaOpenHelper.AUTOR_ID + "='" + Integer.toString(autorID)+"'", null, null, null, null);
                if(c2.getCount()==1 && c2.moveToNext()) {
                    int autorIme = c2.getColumnIndex(BazaOpenHelper.AUTOR_IME);
                    autori.add(new Autor(c2.getString(autorIme), id));
                }
                c2.close();
            }
            c1.close();
            URL x = null;
            try {
                x=new URL(slika);
            }
            catch (Exception e){
            }

            Knjiga k = new Knjiga(idWeb, naziv, autori, opis, datum, x, brStr, pregledana);
            knjige.add(k);
        }
        c.close();
        db.close();
        return knjige;
    }

    public ArrayList<String> dajAutore() {
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            db=this.getReadableDatabase();
        }
        ArrayList<String> kat = new ArrayList<>();
        Cursor c = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, koloneAutor, null, null, null, null, null);
        int INDEX = c.getColumnIndex(BazaOpenHelper.AUTOR_IME);
        while (c.moveToNext()) {
            kat.add(c.getString(INDEX));
        }
        c.close();
        db.close();
        return kat;
    }

    public boolean obojiKnjigu(String id){
        try {
            db=this.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("pregledana", "1");
            db.update(DATABASE_TABLE_KNJ, cv, "idWebServis="+ "'" +id + "'", null);
        }catch (Exception e) {
            db.close();
            return false;
        }
        db.close();
        return true;
    }

    public ArrayList<Knjiga> knjigeKategorije(long idKat){
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            db=this.getReadableDatabase();
        }
        ArrayList<Knjiga> knjige = new ArrayList<>();

        Cursor c = db.query(BazaOpenHelper.DATABASE_TABLE_KNJ, koloneKnjige, BazaOpenHelper.KNJ_ID_KAT + "='" + idKat+"'", null, null, null, null);
        int Iid = c.getColumnIndex(BazaOpenHelper.KNJ_ID);
        int Inaziv = c.getColumnIndex(BazaOpenHelper.KNJ_NAZIV);
        int Iopis = c.getColumnIndex(BazaOpenHelper.KNJ_OPIS);
        int Idatum = c.getColumnIndex(BazaOpenHelper.KNJ_DATUM);
        int IbrStr = c.getColumnIndex(BazaOpenHelper.KNJ_BROJ_STR);
        int IidWeb = c.getColumnIndex(BazaOpenHelper.KNJ_ID_WEB);
        int Islika = c.getColumnIndex(KNJ_SLIKA);
        int Ipreg = c.getColumnIndex(KNJ_PREGLEDANA);
        while (c.moveToNext()) {
            ArrayList<Autor> autori = new ArrayList<>();
            int id = c.getInt(Iid);
            String naziv = c.getString(Inaziv);
            String opis = c.getString(Iopis);
            String datum = c.getString(Idatum);
            String idWeb = c.getString(IidWeb);
            String slika = c.getString(Islika);
            int pregledana = c.getInt(Ipreg);
            int brStr = c.getInt(IbrStr);
            Cursor c1 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneAutorstvo, BazaOpenHelper.AUTORSTVO_KNJIGA + "='" + Integer.toString(id)+"'", null, null, null, null);
            int autor = c1.getColumnIndex(BazaOpenHelper.AUTORSTVO_AUTOR);
            while (c1.moveToNext()) {
                int autorID = c1.getInt(autor);
                Cursor c2 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, koloneAutor, BazaOpenHelper.AUTOR_ID + "='" + Integer.toString(autorID)+"'", null, null, null, null);
                if(c2.getCount()==1 && c2.moveToNext()) {
                    int autorIme = c2.getColumnIndex(BazaOpenHelper.AUTOR_IME);
                    autori.add(new Autor(c2.getString(autorIme), id));
                }
                c2.close();
            }
            c1.close();
            URL x = null;
            try {
                x=new URL(slika);
            }
            catch (Exception e){
            }

            Knjiga k = new Knjiga(idWeb, naziv, autori, opis, datum, x, brStr, pregledana);
            knjige.add(k);
        }
        c.close();
        db.close();
        return knjige;
    }

    public long dajIDKategorije(String kat) {
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            db=this.getReadableDatabase();
        }
        Cursor kategorije = db.query(BazaOpenHelper.DATABASE_TABLE_KAT, koloneKateg, BazaOpenHelper.KAT_NAZIV +"='"+kat+"'", null,null,null,null);
        long idKat=-1;
        if(kategorije.getCount()==1 && kategorije.moveToNext()) {
            idKat = kategorije.getInt(kategorije.getColumnIndex(BazaOpenHelper.KAT_ID));
        }
        kategorije.close();
        return idKat;
    }

    public ArrayList<Knjiga> knjigeAutora(long idAutora){
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            db=this.getReadableDatabase();
        }
        ArrayList<Knjiga> knjige = new ArrayList<>();
        Cursor autorstvo = db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneAutorstvo, BazaOpenHelper.AUTORSTVO_AUTOR + "='" + Long.toString(idAutora)+"'", null, null, null, null);
        int IidKnjige = autorstvo.getColumnIndex(BazaOpenHelper.AUTORSTVO_KNJIGA);
        while (autorstvo.moveToNext()) {
            int idKnjige = autorstvo.getInt(IidKnjige);
            Cursor c = db.query(BazaOpenHelper.DATABASE_TABLE_KNJ, koloneKnjige, BazaOpenHelper.KNJ_ID + "='" + Integer.toString(idKnjige)+"'", null, null, null, null);
            int Inaziv = c.getColumnIndex(BazaOpenHelper.KNJ_NAZIV);
            int Iopis = c.getColumnIndex(BazaOpenHelper.KNJ_OPIS);
            int Idatum = c.getColumnIndex(BazaOpenHelper.KNJ_DATUM);
            int IbrStr = c.getColumnIndex(BazaOpenHelper.KNJ_BROJ_STR);
            int IidWeb = c.getColumnIndex(BazaOpenHelper.KNJ_ID_WEB);
            int Islika = c.getColumnIndex(KNJ_SLIKA);
            int Ipreg = c.getColumnIndex(KNJ_PREGLEDANA);
            ArrayList<Autor> autori = new ArrayList<>();
            if(c.getCount()==1 && c.moveToNext()) {
                String naziv = c.getString(Inaziv);
                String opis = c.getString(Iopis);
                String datum = c.getString(Idatum);
                String idWeb = c.getString(IidWeb);
                String slika = c.getString(Islika);
                int pregledana = c.getInt(Ipreg);
                int brStr = c.getInt(IbrStr);
                Cursor c1 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneAutorstvo, BazaOpenHelper.AUTORSTVO_KNJIGA + "='" + Integer.toString(idKnjige) + "'", null, null, null, null);
                int autor = c1.getColumnIndex(BazaOpenHelper.AUTORSTVO_AUTOR);
                while (c1.moveToNext()) {
                    int autorID = c1.getInt(autor);
                    Cursor c3 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, koloneAutor, BazaOpenHelper.AUTOR_ID + "='" + Integer.toString(autorID) + "'", null, null, null, null);
                    if(c3.getCount()==1 && c3.moveToNext()) {
                        int autorIme = c3.getColumnIndex(BazaOpenHelper.AUTOR_IME);
                        autori.add(new Autor(c3.getString(autorIme), idKnjige));
                    }
                    c3.close();
                }
                c1.close();
                URL x = null;
                try {
                    x=new URL(slika);
                }
                catch (Exception e){
                }

                Knjiga k = new Knjiga(idWeb, naziv, autori, opis, datum, x, brStr, pregledana);
                knjige.add(k);
            }
            c.close();
        }
        autorstvo.close();
        db.close();
        return knjige;
    }

    public long dajIDautora(String imeAutora) {
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            db=this.getReadableDatabase();
        }

        Cursor c2 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, koloneAutor, BazaOpenHelper.AUTOR_IME + "='" + imeAutora+"'", null, null, null, null);
        if(c2.getCount()==1 && c2.moveToNext()) {
            int IautorID = c2.getColumnIndex(BazaOpenHelper.AUTOR_ID);
            long idAutora = c2.getInt(IautorID);
            c2.close();
            return idAutora;
        }
        c2.close();
        return  -1;
    }

    public long dodajKategoriju(String kat){
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            return -1;

        }
        Cursor kategorije = db.query(BazaOpenHelper.DATABASE_TABLE_KAT, koloneKateg, BazaOpenHelper.KAT_NAZIV +"='"+kat+"'", null,null,null,null);
        if(kategorije.getCount()==0) {
            ContentValues novaKat = new ContentValues();
            novaKat.put(BazaOpenHelper.KAT_NAZIV, kat);
            return db.insert(BazaOpenHelper.DATABASE_TABLE_KAT, null, novaKat);

        }
        kategorije.close();
        return -1;
    }

    public long dodajKnjigu(Knjiga k){
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            return -1;

        }
        Cursor knjige = db.query(BazaOpenHelper.DATABASE_TABLE_KNJ, koloneKnjige, BazaOpenHelper.KNJ_ID_WEB +"= '"+k.getId()+"'", null,null,null,null);
        if(knjige.getCount()==0) {
            ContentValues novaKnjiga = new ContentValues();
            novaKnjiga.put(BazaOpenHelper.KNJ_NAZIV, k.getNaziv());
            novaKnjiga.put(BazaOpenHelper.KNJ_OPIS, k.getOpis());
            novaKnjiga.put(BazaOpenHelper.KNJ_DATUM, k.getDatumObjavljivanja());
            novaKnjiga.put(BazaOpenHelper.KNJ_ID_WEB, k.getId());
            if(k.getSlika()!=null) novaKnjiga.put(KNJ_SLIKA, k.getSlika().toString());
            else novaKnjiga.put(KNJ_SLIKA,"");
            novaKnjiga.put(KNJ_PREGLEDANA, k.getPregledana());
            Cursor kategorije = db.query(BazaOpenHelper.DATABASE_TABLE_KAT, koloneKateg, BazaOpenHelper.KAT_NAZIV + "= '" + k.getKategorija()+"'", null, null, null, null);
            if(kategorije.getCount()==1 && kategorije.moveToNext()) {

                    int idKat = kategorije.getInt(kategorije.getColumnIndex(BazaOpenHelper.KAT_ID));
                    kategorije.close();
                    novaKnjiga.put(BazaOpenHelper.KNJ_ID_KAT, idKat);

                    knjige.close();
                    return db.insert(BazaOpenHelper.DATABASE_TABLE_KNJ, null, novaKnjiga);

            }
        }
        knjige.close();
        return -1;
    }

    public long dodajAutora(String ime){
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            return -1;
        }
        Cursor autori = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, koloneAutor, BazaOpenHelper.AUTOR_IME +"= '"+ime+"'", null,null,null,null);
        if(autori.getCount()==0) {
            ContentValues noviAutor = new ContentValues();
            noviAutor.put(BazaOpenHelper.AUTOR_IME, ime);
            return db.insert(BazaOpenHelper.DATABASE_TABLE_AUTOR, null, noviAutor);
        }
        autori.close();
        return -2;
    }

    public long dodajAutorstvo(String imeAutora, String webIDknjige){
        try {
            db=this.getWritableDatabase();
        }catch (Exception e){
            return -1;

        }
        Cursor autori = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, koloneAutor, BazaOpenHelper.AUTOR_IME +"='"+imeAutora+"'", null,null,null,null);
        Cursor knjige = db.query(BazaOpenHelper.DATABASE_TABLE_KNJ, koloneKnjige, BazaOpenHelper.KNJ_ID_WEB +"='"+webIDknjige+"'", null,null,null,null);

        if(autori.getCount()==1 && autori.moveToNext() && knjige.getCount()==1 && knjige.moveToNext()) {
            int idAutor = autori.getInt(autori.getColumnIndex(BazaOpenHelper.AUTOR_ID));
            autori.close();

            int idKnjige = knjige.getInt(autori.getColumnIndex(BazaOpenHelper.AUTOR_ID));
            knjige.close();


            ContentValues novoAutorstvo = new ContentValues();
            novoAutorstvo.put(BazaOpenHelper.AUTORSTVO_AUTOR, idAutor);
            novoAutorstvo.put(BazaOpenHelper.AUTORSTVO_KNJIGA, idKnjige);
            return db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, novoAutorstvo);
        }
        return -1;
    }

}

