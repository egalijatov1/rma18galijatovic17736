package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by USAID on 06-Apr-18.
 */

public class AutorAdapter extends ArrayAdapter {
    public AutorAdapter(Context context, ArrayList<Autor> autori) {
        super(context, 0, autori);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Autor k=(Autor)getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.lista_autor, parent, false);
        }
        // Lookup view for data population
        TextView tvNaziv = (TextView) convertView.findViewById(R.id.eNaziv);
        TextView tvAutor = (TextView) convertView.findViewById(R.id.eAutor);

        tvNaziv.setText(k.getImeiPrezime());
        //tvAutor.setText(Integer.toString(k.getBrojKnjiga()));

        return convertView;
    }
}
