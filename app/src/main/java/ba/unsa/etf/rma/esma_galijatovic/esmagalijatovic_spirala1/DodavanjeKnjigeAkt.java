package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DodavanjeKnjigeAkt extends AppCompatActivity {
    String nazivSlike="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);
        ImageView slika = (ImageView)findViewById(R.id.naslovnaStr) ;
        slika.setImageIcon(null);
        final Spinner spiner= (Spinner)findViewById(R.id.sKategorijaKnjige);
        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, KategorijeAkt.kategorije);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner.setAdapter(adp1);

        //upisivanje knjige u listu
        Button upisi = (Button)findViewById(R.id.dUpisiKnjigu);
        upisi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText autor = (EditText)findViewById(R.id.imeAutora);

                EditText naziv = (EditText)findViewById(R.id.nazivKnjige);
                ImageView slika = (ImageView)findViewById(R.id.naslovnaStr) ;
                if(!naziv.getText().toString().isEmpty() && !autor.getText().toString().isEmpty()) {
                    KategorijeAkt.upisaneKnjige.add(0, new Knjiga(naziv.getText().toString(), autor.getText().toString(), spiner.getSelectedItem().toString(), nazivSlike));
                    naziv.setText("");
                    autor.setText("");
                    slika.setImageIcon(null);
                    Toast.makeText(getBaseContext(),"Uspjesno unesena knjiga!", Toast.LENGTH_SHORT).show();

                }
                else{
                    Toast.makeText(getBaseContext(),"Unesite potrebne podatke!", Toast.LENGTH_SHORT).show();

                }

            }
        });


        // dohvacanje slike
        Button slikaDugme = (Button)findViewById(R.id.dNadjiSliku);
        slikaDugme.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText naziv = (EditText)findViewById(R.id.nazivKnjige);
                if(!naziv.getText().toString().isEmpty()) {
                    Intent imageIntent = new Intent();
                    imageIntent.setAction(Intent.ACTION_GET_CONTENT);
                    imageIntent.setType("image/*");
                    // Provjera da li postoji aplikacija koja može obaviti navedenu akciju
                    if (imageIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(Intent.createChooser(imageIntent, "Odaberite sliku:"), 1);

                    }
                }
                else{
                    Toast.makeText(getBaseContext(),"Prvo unesite naziv knjige!", Toast.LENGTH_SHORT).show();
                }



            }
        });

        //zatvaranje aktivnosti na ponisti
        Button ponisti= (Button)findViewById(R.id.dPonisti);
        ponisti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode==RESULT_CANCELED)
        {
            // action cancelled
        }
        if(resultCode==RESULT_OK)
        {
            Uri selectedimg = data.getData();
            FileOutputStream outputStream;
            EditText et=(EditText)findViewById(R.id.nazivKnjige);
            if(!et.getText().toString().isEmpty()){
                nazivSlike=et.getText().toString();
            }
            try{
                outputStream = openFileOutput(nazivSlike, Context.MODE_PRIVATE);
                try{
                    getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                    outputStream.close();
                } catch(IOException ex){
                    Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, ex);
                }
                ImageView slika= (ImageView)findViewById(R.id.naslovnaStr) ;
                slika.setImageBitmap(BitmapFactory.decodeStream(openFileInput(nazivSlike)));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
