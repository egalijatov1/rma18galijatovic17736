package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by USAID on 18-May-18.
 */

public class KnjigePoznanika extends IntentService {
    final String API_KEY="AIzaSyCWsmbRBqcj1ycgw2Ki96PT0yx4OBMKBro";
    public final static int STATUS_START=0;
    public final static int STATUS_FINISH=1;
    public final static int STATUS_ERROR=2;
    ArrayList<Knjiga> rez= new ArrayList<>();
    public KnjigePoznanika(){
        super(null);
    }
    public KnjigePoznanika(String name){
        super(name);
    }
    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent){
        Bundle bundle = new Bundle();
        final ResultReceiver reciever = intent.getParcelableExtra("receiver");
        reciever.send(STATUS_START, Bundle.EMPTY);

        String userID= intent.getStringExtra("userID");

        String query = null;
        try {
            query = URLEncoder.encode(userID, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            reciever.send(STATUS_ERROR, Bundle.EMPTY);
        }
        String url1 = "https://www.googleapis.com/books/v1/users/" + query + "/bookshelves?key=" + API_KEY;
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject bookshelves = new JSONObject(rezultat);
            JSONArray itemsPolice= bookshelves.getJSONArray("items");



            for (int i = 0; i < itemsPolice.length(); i++) {
                JSONObject polica = itemsPolice.getJSONObject(i);
                String id=polica.getString("id");

                String url2="https://www.googleapis.com/books/v1/users/" + query + "/bookshelves/" + id +"/volumes?key=" + API_KEY;
                try {
                    URL url3 = new URL(url2);
                    HttpURLConnection urlConnection1 = (HttpURLConnection) url3.openConnection();
                    InputStream in1 = new BufferedInputStream(urlConnection1.getInputStream());
                    String rezultat1 = convertStreamToString(in1);
                    JSONObject books = new JSONObject(rezultat1);
                    try{
                    JSONArray items = books.getJSONArray("items");

                    for (int j = 0; j < items.length(); j++) {
                        JSONObject knjiga = items.getJSONObject(j);
                        JSONObject book = knjiga.getJSONObject("volumeInfo");
                        String name = book.getString("title");
                        String book_id = knjiga.getString("id");
                        String description = "";
                        String datum="";
                        int brojStr=0;
                        JSONObject zaSliku=null;
                        URL image=null;
                        ArrayList<Autor> autors = new ArrayList<>();
                        String imeAutora="";
                        try {
                            description = book.getString("description");
                        }catch (JSONException e){}
                        try {
                            datum = book.getString("publishedDate");
                        }catch (JSONException e){}
                        try {
                            brojStr = book.getInt("pageCount");
                        }catch (JSONException e){}
                        try {
                            zaSliku = book.getJSONObject("imageLinks");
                            if (zaSliku != null) {
                                String slika = zaSliku.getString("thumbnail");
                                image = new URL(slika);
                            }
                        }catch (JSONException e){}
                        try {
                            JSONArray autori = book.getJSONArray("authors");

                            for(int k=0; k<autori.length();k++){
                                autors.add(new Autor(autori.getString(k), book_id));
                                String plus = autori.getString(k)+" ";
                                imeAutora = imeAutora.concat(plus);
                            }
                        } catch (JSONException e){}
                        Knjiga knjiga1=new Knjiga(book_id, name, autors, description, datum, image, brojStr);
                        knjiga1.setImeAutora(imeAutora);
                        rez.add(knjiga1);
                    }
                    }
                    catch(JSONException e){
                        //polica nema knjiga
                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                    reciever.send(STATUS_ERROR, Bundle.EMPTY);
                } catch (IOException e) {
                    e.printStackTrace();
                    reciever.send(STATUS_ERROR, Bundle.EMPTY);
                } catch (JSONException e) {
                    e.printStackTrace();
                    reciever.send(STATUS_ERROR, Bundle.EMPTY);
                }

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            reciever.send(STATUS_ERROR, Bundle.EMPTY);
        } catch (IOException e) {
            e.printStackTrace();
            reciever.send(STATUS_ERROR, Bundle.EMPTY);
        } catch (JSONException e) {
            e.printStackTrace();
            reciever.send(STATUS_ERROR, Bundle.EMPTY);
        }

        bundle.putParcelableArrayList("result", rez);
        reciever.send(STATUS_FINISH, bundle);


    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

}
