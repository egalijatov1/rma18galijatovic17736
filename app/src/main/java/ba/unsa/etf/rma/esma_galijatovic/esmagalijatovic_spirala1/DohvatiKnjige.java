package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by USAID on 17-May-18.
 */

public class DohvatiKnjige extends AsyncTask<String, Integer, Void> {
    public interface IDohvatiKnjigeDone{
        public void onDohvatiDone(ArrayList<Knjiga> rez);
    }
    public ArrayList<Knjiga> rez=new ArrayList<>();
    private IDohvatiKnjigeDone pozivatelj;
    public DohvatiKnjige(IDohvatiKnjigeDone p) {pozivatelj = p;};

    @Override
    protected Void doInBackground(String... params){
        for (String s: params
             ) {
            String query = null;
            try {
                query = URLEncoder.encode(s, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String url1 = "https://www.googleapis.com/books/v1/volumes?q=intitle:" + query + "&maxResults=5";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject books = new JSONObject(rezultat);

                JSONArray items= books.getJSONArray("items");
                for (int i = 0; i < items.length(); i++) {
                    JSONObject knjiga = items.getJSONObject(i);
                    JSONObject book = knjiga.getJSONObject("volumeInfo");
                    String name = book.getString("title");
                    String book_id = knjiga.getString("id");
                    String description = "";
                    String datum="";
                    int brojStr=0;
                    JSONObject zaSliku=null;
                    URL image=null;
                    ArrayList<Autor> autors = new ArrayList<>();
                    String imeAutora ="";

                    try {
                        description = book.getString("description");
                    }catch (JSONException e){}
                    try {
                        datum = book.getString("publishedDate");
                    }catch (JSONException e){}
                    try {
                        brojStr = book.getInt("pageCount");
                    }catch (JSONException e){}
                    String slika="";
                    try {
                        zaSliku = book.getJSONObject("imageLinks");
                        if (zaSliku != null) {
                            slika = zaSliku.getString("thumbnail");
                            image = new URL(slika);
                        }
                    }catch (JSONException e){}
                    try {
                        JSONArray autori = book.getJSONArray("authors");

                        for(int j=0; j<autori.length();j++){
                            autors.add(new Autor(autori.getString(j), book_id));

                                String plus=autori.getString(j)+" ";
                                imeAutora=imeAutora.concat(plus);

                        }

                    } catch (JSONException e){}

                    Knjiga k=new Knjiga(book_id, name, autors, description, datum, image, brojStr);
                    k.setImeAutora(imeAutora);
                    //k.setSlika1(slika);
                    rez.add(k);

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        return  null;
    }


    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onDohvatiDone(rez);
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

}
