package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.security.spec.ECField;
import java.util.ArrayList;

/**
 * Created by USAID on 27-Mar-18.
 */

public class Knjiga implements Parcelable {
    private String id;
    private String naziv;
    private ArrayList<Autor> autori;
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica;
    private int pregledana;

    //stari atributi

    private String imeAutora;
    private String kategorija;
    private String slika1;
    private boolean obojena;

    public Knjiga(){}

    @SuppressWarnings("unchecked")
    protected Knjiga(Parcel p){
        id=p.readString();
        naziv=p.readString();
        opis=p.readString();
        datumObjavljivanja=p.readString();
        try {
            slika = new URL(p.readString());
        }catch (Exception e){}
        brojStranica=p.readInt();
        autori = p.readArrayList(Autor.class.getClassLoader());

    }
    public static final Creator<Knjiga> CREATOR=new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel parcel) {
            return new Knjiga(parcel);
        }

        @Override
        public Knjiga[] newArray(int i) {
            return new Knjiga[i];
        }
    };
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(id);
        dest.writeString(naziv);
        dest.writeString(opis);
        dest.writeString(datumObjavljivanja);
        dest.writeString(slika.toString());
        dest.writeInt(brojStranica);
        dest.writeList(autori);
    }

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica){
        this.id=id;
        this.naziv= naziv;
        this.autori=autori;
        this.opis=opis;
        this.datumObjavljivanja=datumObjavljivanja;
        this.slika=slika;
        this.brojStranica=brojStranica;
        imeAutora="";
        for (Autor a: autori
             ) {
            imeAutora= imeAutora.concat(a.getImeiPrezime()+" ");
        }
        this.pregledana=0;
    }
    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica, int pregledana){
        this.id=id;
        this.naziv= naziv;
        this.autori=autori;
        this.opis=opis;
        this.datumObjavljivanja=datumObjavljivanja;
        this.slika=slika;
        this.brojStranica=brojStranica;
        imeAutora="";
        for (Autor a: autori
                ) {
            imeAutora= imeAutora.concat(a.getImeiPrezime()+" ");
        }
        this.pregledana=pregledana;
    }


    public boolean isObojena() {
        return obojena;
    }

    public void setObojena(boolean obojena) {
        this.obojena = obojena;
    }

    public Knjiga(String naziv, String imeAutora, String kategorija, String slika) {
        this.naziv = naziv;
        this.imeAutora = imeAutora;
        this.kategorija = kategorija;
        this.slika1 = slika;
    }

    public void setNaziv(String ime){
        naziv=ime;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setImeAutora(String imeAutora) {
        this.imeAutora = imeAutora;
    }

    public String getImeAutora() {
        return imeAutora;
    }

    public String getKategorija() {
        return kategorija;
    }

    public String getSlika1() {
        return slika1;
    }

    public void setSlika1(String slika) {
        this.slika1 = slika;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public int getPregledana() {
        return pregledana;
    }

    public void setPregledana(int pregledana) {
        this.pregledana = pregledana;
    }
}

