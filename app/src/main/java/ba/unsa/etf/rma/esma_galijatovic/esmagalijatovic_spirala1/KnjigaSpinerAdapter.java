package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by USAID on 18-May-18.
 */

public class KnjigaSpinerAdapter extends ArrayAdapter<Knjiga> {
    public KnjigaSpinerAdapter(Context context, ArrayList<Knjiga> knjige) {
        super(context, 0, knjige);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Knjiga k = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.lista_autor, parent, false);
        }
        // Lookup view for data population
        TextView tvNaziv = (TextView) convertView.findViewById(R.id.eNaziv);
        TextView tvAutor = (TextView) convertView.findViewById(R.id.eAutor);

        tvNaziv.setText(k.getNaziv());
        if(getItem(position).getAutori().isEmpty()){
            tvAutor.setText("");
        }
        else {
            String ispis="";
            ArrayList<Autor> x=getItem(position).getAutori();
            for (Autor a: x
                    ) {
                String plus=a.getImeiPrezime()+" ";
                ispis=ispis.concat(plus);
            }
            tvAutor.setText(ispis);
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView);
    }

    private View initView(int position, View convertView) {
        if(convertView == null)
            convertView = View.inflate(getContext(), android.R.layout.simple_list_item_2, null);
        TextView tvText1 = (TextView)convertView.findViewById(android.R.id.text1);
        TextView tvText2 = (TextView)convertView.findViewById(android.R.id.text2);
        tvText1.setText(getItem(position).getNaziv());
        tvText2.setText(getItem(position).getImeAutora());

        return convertView;
    }
}
