package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by USAID on 19-May-18.
 */

public class CustomResultReceiver extends ResultReceiver  {
    private Receiver mReceiver;
    public interface Receiver{
        public void onReceiveResult(int resultCode, Bundle resultData);
    }
    public CustomResultReceiver(Handler handler){
        super(handler);
    }
    public void setReceiver(Receiver receiver){
        mReceiver=receiver;
    }
    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }

}

