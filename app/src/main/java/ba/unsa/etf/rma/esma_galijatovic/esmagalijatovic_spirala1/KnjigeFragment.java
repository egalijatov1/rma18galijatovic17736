package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by USAID on 06-Apr-18.
 */

public class KnjigeFragment extends Fragment {
    public static ArrayList<Knjiga> knjige;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.filter_knjige_layout, container, false);

    }
    @Override
    public  void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getArguments()!=null){
        if(getArguments().containsKey("zanr") || getArguments().containsKey("autor")) {
            //povratak na pocetnu formu
            Button povratak= (Button)getView().findViewById(R.id.dPovratak);
            if(KategorijeAkt.siriL){
                povratak.setVisibility(View.GONE);
            }
            povratak.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    getFragmentManager().popBackStack();
                }
            });

            knjige= new ArrayList<Knjiga>();
            if(!KategorijeAkt.upisaneKnjige.isEmpty()) {
                if(getArguments().containsKey("zanr")) {
                    String zanr=getArguments().getString("zanr");
                    long id = KategorijeAkt.openHelper.dajIDKategorije(zanr);
                    knjige = KategorijeAkt.openHelper.knjigeKategorije(id);
                    //knjige = KategorijeAkt.upisaneKnjige;
                }
                else if(getArguments().containsKey("autor")){
                    String autor=getArguments().getString("autor");
                    long id = KategorijeAkt.openHelper.dajIDautora(autor);
                    knjige = KategorijeAkt.openHelper.knjigeAutora(id);
                }
            }

            KnjigaAdapter adapter= new KnjigaAdapter(getActivity(), knjige, new Klik(){
                @Override
                public void onBtnClick(int position) {

                    FragmentManager fm=getFragmentManager();
                    FragmentPreporuci f1=null;//(KnjigeFragment) fm.findFragmentByTag("Knjiga");
                    if(f1==null) {
                        f1 = new FragmentPreporuci();
                    }
                    Bundle argumenti= new Bundle();
                    argumenti.putParcelable("knjiga", KnjigeFragment.knjige.get(position));
                    f1.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjesto1, f1, "Knjige1").addToBackStack(null).commit();

                }
            });
            ListView lista = (ListView)getView().findViewById(R.id.listaKnjiga);
            lista.setAdapter(adapter);

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    knjige.get(position).setPregledana(1);
                    KategorijeAkt.openHelper.obojiKnjigu(knjige.get(position).getId());
                    view.setBackgroundColor(getActivity().getColor(R.color.element_knjiga));
                    knjige.get(position).setPregledana(1);

                }
            });

        }
    }
    }
}
