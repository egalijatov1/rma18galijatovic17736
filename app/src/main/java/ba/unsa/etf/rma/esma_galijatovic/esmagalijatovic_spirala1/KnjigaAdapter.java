package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by USAID on 28-Mar-18.
 */

public class KnjigaAdapter extends ArrayAdapter<Knjiga> {
    private Klik mojKlik = null;
    public KnjigaAdapter(Context context, ArrayList<Knjiga> knjige, Klik klik) {
        super(context, 0, knjige);
        mojKlik = klik;
    }
    public KnjigaAdapter(Context context, ArrayList<Knjiga> knjige) {
        super(context, 0, knjige);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Knjiga k = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.lista_knjiga, parent, false);
        }
        if(k.getPregledana()==1){
            convertView.setBackgroundColor(getContext().getColor(R.color.element_knjiga));
        }
        // Lookup view for data population
        TextView tvNaziv = (TextView) convertView.findViewById(R.id.eNaziv);
        TextView tvAutor = (TextView) convertView.findViewById(R.id.eAutor);
        TextView tvOpis = (TextView)convertView.findViewById(R.id.eOpis);
        TextView tvDatum = (TextView)convertView.findViewById(R.id.eDatumObjavljivanja);
        TextView tvBroj = (TextView)convertView.findViewById(R.id.eBrojStranica);
        ImageView iv = (ImageView)convertView.findViewById(R.id.eNaslovna);
        Button preporuci =(Button)convertView.findViewById(R.id.dPreporuci);
        preporuci.setTag(position);
        preporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mojKlik!=null){
                    mojKlik.onBtnClick((Integer)v.getTag());
                }
            }});
        int x= k.getBrojStranica();
        tvNaziv.setText(k.getNaziv());
        tvAutor.setText(k.getImeAutora());
        tvOpis.setText(k.getOpis());
        tvDatum.setText(k.getDatumObjavljivanja());
        tvBroj.setText(Integer.toString( k.getBrojStranica()));
        if(k.getSlika()!=null) {
            Picasso.get().load(k.getSlika().toString()).into(iv);
        }
        else {
            Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(iv);
        }
        return convertView;
    }
}

