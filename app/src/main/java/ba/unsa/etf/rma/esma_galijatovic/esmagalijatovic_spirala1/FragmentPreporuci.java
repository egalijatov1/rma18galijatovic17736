package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.Manifest;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by USAID on 26-May-18.
 */

public class FragmentPreporuci extends Fragment {
    private ArrayList<String> emailovi = new ArrayList<>();
    private ArrayList<String> imena = new ArrayList<>();
    final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS=1;
    Knjiga k;
    String email;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.preporuci_layout, container, false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            k = getArguments().getParcelable("knjiga");
            TextView tvNaziv = (TextView) getView().findViewById(R.id.eNaziv);
            TextView tvAutor = (TextView) getView().findViewById(R.id.eAutor);
            TextView tvOpis = (TextView) getView().findViewById(R.id.eOpis);
            TextView tvDatum = (TextView) getView().findViewById(R.id.eDatumObjavljivanja);
            TextView tvBroj = (TextView) getView().findViewById(R.id.eBrojStranica);
            ImageView iv = (ImageView) getView().findViewById(R.id.eNaslovna);
            tvNaziv.setText(k.getNaziv());
            tvAutor.setText(k.getImeAutora());
            tvOpis.setText(k.getOpis());
            tvDatum.setText(k.getDatumObjavljivanja());
            tvBroj.setText(Integer.toString(k.getBrojStranica()));
            if (k.getSlika() != null) {
                Picasso.get().load(k.getSlika().toString()).into(iv);
            } else {
                Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(iv);
            }
            Button posalji = (Button) getView().findViewById(R.id.dPosalji);
            procitajKontakte();
            final Spinner spinner = (Spinner) getView().findViewById(R.id.sKontakti);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, imena);
            spinner.setAdapter(adapter);


            posalji.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(spinner.getSelectedItem()!=null) {
                        email = "Zdravo " + spinner.getSelectedItem().toString() + ",\n Procitaj knjigu " + k.getNaziv() + " od " + k.getImeAutora() + "!";
                    }
                    if(email!=null) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{emailovi.get(spinner.getSelectedItemPosition())});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Preporuka knjige");
                        i.putExtra(Intent.EXTRA_TEXT, email);
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

        }
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void procitajKontakte() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.READ_CONTACTS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                String[] s= {Manifest.permission.READ_CONTACTS};
                ActivityCompat.requestPermissions(getActivity(), s, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted

            ContentResolver cr = getActivity().getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    Cursor cur1 = cr.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (cur1.moveToNext()) {
                        //to get the contact names
                        String name = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                        String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                        if (email != null) {
                            emailovi.add(email);
                            imena.add(name);
                        }
                    }
                    cur1.close();
                }
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,@NonNull String permissions[],
                                           @NonNull int [] grantResults)

    {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (PackageManager.PERMISSION_GRANTED == (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS)))

                    {
                        procitajKontakte();
                    } else {
                        getFragmentManager().popBackStack();
                    }

                }
            }
        }
    }
}
