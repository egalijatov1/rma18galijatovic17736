package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by USAID on 05-Apr-18.
 */

public class ListeFragment extends Fragment {
    private ArrayAdapter<String> adapter;
    private ArrayList<String> kategorije;
    private ArrayList<Autor> autori;
    private ArrayList<String> autoriString;
    public static ArrayList<Knjiga> upisaneKnjige;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_liste, container, false);

    }
    @Override
    public  void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getArguments()!=null && getArguments().containsKey("lista_kategorija")){
            kategorije= getArguments().getStringArrayList("lista_kategorija");
            final ListView lw=(ListView)getView().findViewById(R.id.listaKategorija);
            adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, kategorije);
            lw.setAdapter(adapter);
            final Button dugme = (Button)getView().findViewById(R.id.dDodajKategoriju);
            dugme.setEnabled(false);


            // dugme dodaj kategoriju ako je lista prazna nakon pretrage
            dugme.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    EditText unos = (EditText)getView().findViewById(R.id.tekstPretraga);
                    if(!unos.getText().toString().isEmpty()) {
                        long error=KategorijeAkt.openHelper.dodajKategoriju(unos.getText().toString());
                        if(error==-1){
                            Toast.makeText(getActivity(),"Greska!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        unos.setText("");
                        KategorijeAkt.kategorije.add(0, unos.getText().toString());
                        kategorije.add(0, unos.getText().toString());
                        dugme.setEnabled(false);
                        adapter= new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,kategorije);
                        lw.setAdapter(adapter);
                    }
                }
            });

            Button pretraga= (Button)getView().findViewById(R.id.dPretraga);

            // dugme pretraga
            pretraga.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    EditText tekst=(EditText)getView().findViewById(R.id.tekstPretraga);
                    ArrayList<String> rezultatPretrage= new ArrayList<String>();
                    adapter.getFilter().filter(tekst.getText().toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if (count == 0) {
                                dugme.setEnabled(true);
                            }
                        }
                    });
                    //}

                }
            });



            Button dugmeDodajKnjigu = (Button)getView().findViewById(R.id.dDodajKnjigu);
            // dodaj knjigu klik
            dugmeDodajKnjigu.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    // IMPLEMENTIRATI KLIK
                    FragmentManager fm = getFragmentManager();

                    if(!KategorijeAkt.siriL) {
                        DodavanjeKnjigeFragment f1=null;//(DodavanjeKnjigeFragment) fm.findFragmentByTag("dodavanjeKnjige");
                        if(f1==null){
                            f1=new DodavanjeKnjigeFragment();
                        }
                        fm.beginTransaction().replace(R.id.mjesto1, f1, "dodavanjeKnjige").addToBackStack(null).commit();
                    }
                    else{
                        DodavanjeKnjigeFragment f1=null;//(DodavanjeKnjigeFragment) fm.findFragmentByTag("dodavanjeKnjige1");
                        if(f1==null){
                            f1=new DodavanjeKnjigeFragment();
                        }
                        FrameLayout mjesto1=(FrameLayout)getActivity().findViewById(R.id.mjestoF1);
                        FrameLayout mjesto2=(FrameLayout)getActivity().findViewById(R.id.mjestoF2);
                        if(mjesto1!=null)mjesto1.setVisibility(View.INVISIBLE);
                        if(mjesto2!=null)mjesto2.setVisibility(View.INVISIBLE);
                        FrameLayout mjesto3=(FrameLayout)getActivity().findViewById(R.id.mjestoF3);
                        if(mjesto3!=null) mjesto3.setVisibility(View.VISIBLE);
                        fm.beginTransaction().replace(R.id.mjestoF3,f1,"dodavanjeKnjige1").commit();


                    }

                }
            });


            // otvaranje liste knjiga klikom na zanr
            lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                    FragmentManager fm=getFragmentManager();
                    KnjigeFragment f1= new KnjigeFragment();

                    Bundle argumenti= new Bundle();
                    argumenti.putString("zanr", KategorijeAkt.kategorije.get(position));
                    f1.setArguments(argumenti);
                    if(!KategorijeAkt.siriL) {
                        fm.beginTransaction().replace(R.id.mjesto1, f1, "Knjige1").addToBackStack(null).commit();
                    }
                    else{
                        fm.beginTransaction().replace(R.id.mjestoF2, f1, "Knjige1").commit();

                    }

                }
            });

        }
        else if(getArguments()!=null && getArguments().containsKey("lista_autora")){
            EditText ed = (EditText)getView().findViewById(R.id.tekstPretraga);
            Button dugmeDodajKnjigu = (Button)getView().findViewById(R.id.dDodajKnjigu);
            Button pretraga= (Button)getView().findViewById(R.id.dPretraga);
            Button dugme = (Button)getView().findViewById(R.id.dDodajKategoriju);
            ListView lw=(ListView)getView().findViewById(R.id.listaKategorija);
            dugme.setVisibility(View.GONE);
            pretraga.setVisibility(View.GONE);
            dugmeDodajKnjigu.setVisibility(View.GONE);
            ed.setVisibility(View.GONE);
            autoriString = getArguments().getStringArrayList("lista_autora");
            ArrayAdapter adapter1 =  new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, autoriString);;
            lw.setAdapter(adapter1);
            // otvaranje liste knjiga klikom na autora
            lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                    FragmentManager fm=getFragmentManager();
                    KnjigeFragment f1=null;//(KnjigeFragment) fm.findFragmentByTag("Knjiga");
                    if(f1==null) {
                        f1 = new KnjigeFragment();
                    }
                    Bundle argumenti= new Bundle();
                    argumenti.putString("autor", KategorijeAkt.autoriString.get(position));
                    f1.setArguments(argumenti);
                    if(!KategorijeAkt.siriL) {
                        fm.beginTransaction().replace(R.id.mjesto1, f1, "Knjige1").addToBackStack(null).commit();
                    }
                    else{
                        fm.beginTransaction().replace(R.id.mjestoF2, f1, "Knjige1").commit();

                    }

                }
            });

        }
    }
}
