package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.graphics.Color;
import android.graphics.drawable.PaintDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaKnjigaAkt extends AppCompatActivity {
    private ArrayList<Knjiga> knjige;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);

        //povratak na pocetnu formu
        Button povratak= (Button)findViewById(R.id.dPovratak);
        povratak.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        knjige= new ArrayList<Knjiga>();
        Bundle extras = getIntent().getExtras();
        String zanr = extras.getString("zanr");
        if(!KategorijeAkt.upisaneKnjige.isEmpty()) {
            for (Knjiga k : KategorijeAkt.upisaneKnjige
                    ) {
                if (k.getKategorija().equals(zanr)) {
                    knjige.add(0, k);
                }
            }
        }

        KnjigaAdapter adapter= new KnjigaAdapter(this, knjige);
        ListView lista = (ListView)findViewById(R.id.listaKnjiga);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                view.setBackgroundColor(getBaseContext().getColor(R.color.element_knjiga));
                knjige.get(position).setObojena(true);
                for (Knjiga k: KategorijeAkt.upisaneKnjige
                     ) {
                    if(k.getNaziv().equals(knjige.get(position).getNaziv())){
                        k.setObojena(true);
                    }
                }

            }
        });

    }
}
