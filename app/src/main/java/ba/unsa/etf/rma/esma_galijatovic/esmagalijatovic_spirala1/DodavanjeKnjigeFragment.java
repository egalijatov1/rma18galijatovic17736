package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by USAID on 06-Apr-18.
 */

public class DodavanjeKnjigeFragment extends Fragment {
    String nazivSlike="";
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.activity_dodavanje_knjige_akt, container, false);

    }
    @Override
    public  void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ImageView slika = (ImageView)getView().findViewById(R.id.naslovnaStr) ;



        slika.setImageIcon(null);
        final Spinner spiner= (Spinner)getView().findViewById(R.id.sKategorijaKnjige);
        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, KategorijeAkt.kategorije);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner.setAdapter(adp1);

        //upisivanje knjige u listu
        Button upisi = (Button)getView().findViewById(R.id.dUpisiKnjigu);
        upisi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText autor = (EditText)getView().findViewById(R.id.imeAutora);

                EditText naziv = (EditText)getView().findViewById(R.id.nazivKnjige);
                ImageView slika = (ImageView)getView().findViewById(R.id.naslovnaStr) ;
                if(!naziv.getText().toString().isEmpty() && !autor.getText().toString().isEmpty()) {
                    KategorijeAkt.upisaneKnjige.add(0, new Knjiga(naziv.getText().toString(), autor.getText().toString(), spiner.getSelectedItem().toString(), nazivSlike));
                    Boolean imaAutor=false;
                    for (Autor a: KategorijeAkt.autori
                         ) {
                        if(a.getImeiPrezime().equals(autor.getText().toString())){
                            a.setBrojKnjiga(a.getBrojKnjiga()+1);
                            imaAutor=true;                        }
                    }
                    if(!imaAutor){
                        KategorijeAkt.autori.add(0,new Autor(autor.getText().toString(), 1));
                    }
                    naziv.setText("");
                    autor.setText("");
                    slika.setImageIcon(null);
                    Toast.makeText(getActivity(),R.string.Suspjesno, Toast.LENGTH_SHORT).show();

                }
                else{
                    Toast.makeText(getActivity(),R.string.SprvoIme, Toast.LENGTH_SHORT).show();

                }

            }
        });


        // dohvacanje slike
        Button slikaDugme = (Button)getView().findViewById(R.id.dNadjiSliku);
        slikaDugme.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText naziv = (EditText)getView().findViewById(R.id.nazivKnjige);
                if(!naziv.getText().toString().isEmpty()) {
                    Intent imageIntent = new Intent();
                    imageIntent.setAction(Intent.ACTION_GET_CONTENT);
                    imageIntent.setType("image/*");
                    // Provjera da li postoji aplikacija koja može obaviti navedenu akciju
                    if (imageIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(Intent.createChooser(imageIntent, "Odaberite sliku:"), 1);

                    }
                }
                else{
                    Toast.makeText(getActivity(),R.string.SprvoIme, Toast.LENGTH_SHORT).show();
                }



            }
        });

        //zatvaranje aktivnosti na ponisti
        Button ponisti= (Button)getView().findViewById(R.id.dPonisti);

        ponisti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(KategorijeAkt.siriL) {
                    FrameLayout mjesto1 = (FrameLayout) getActivity().findViewById(R.id.mjestoF1);
                    FrameLayout mjesto2 = (FrameLayout) getActivity().findViewById(R.id.mjestoF2);
                    mjesto1.setVisibility(View.VISIBLE);
                    mjesto2.setVisibility(View.VISIBLE);
                    FrameLayout mjesto3 = (FrameLayout) getActivity().findViewById(R.id.mjestoF3);
                    mjesto3.setVisibility(View.INVISIBLE);
                }
                else {
                    getFragmentManager().popBackStack();
                }
            }
        });
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode==RESULT_CANCELED)
        {
            // action cancelled
        }
        if(resultCode==RESULT_OK)
        {
            Uri selectedimg = data.getData();
            FileOutputStream outputStream;
            EditText et=(EditText)getView().findViewById(R.id.nazivKnjige);
            if(!et.getText().toString().isEmpty()){
                nazivSlike=et.getText().toString();
            }
            try{
                outputStream = getActivity().openFileOutput(nazivSlike, Context.MODE_PRIVATE);
                try{
                    getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                    outputStream.close();
                } catch(IOException ex){
                    Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, ex);
                }
                ImageView slika= (ImageView)getView().findViewById(R.id.naslovnaStr) ;
                slika.setImageBitmap(BitmapFactory.decodeStream(getActivity().openFileInput(nazivSlike)));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
