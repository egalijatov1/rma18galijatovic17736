package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;



/**
 * Created by USAID on 18-May-18.
 */

public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone, CustomResultReceiver.Receiver{
    ArrayList<Knjiga> knjige;
    private boolean split=false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.online_layout, container, false);

    }
    @Override
    public  void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Spinner spinerKat=(Spinner)getView().findViewById(R.id.sKategorije);
        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, KategorijeAkt.kategorije);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerKat.setAdapter(adp1);

        knjige=new ArrayList<>();
        final Spinner spinerRez=(Spinner)getView().findViewById(R.id.sRezultat);
        KnjigaSpinerAdapter adapter= new KnjigaSpinerAdapter(getContext(), knjige);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerRez.setAdapter(adapter);

        Button search=(Button)getView().findViewById(R.id.dRun);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tv=(EditText)getView().findViewById(R.id.textUpit);
                String upit= tv.getText().toString();

                if(upit.contains(";")){
                    split=false;
                    String[] upiti = upit.split(";");
                    for (String s: upiti
                         ) {
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(s);
                        split=true;
                    }
                }
                else if(upit.length()>6 && upit.substring(0,6).equals("autor:")){
                    String autor=upit.substring(6);
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(autor);
                    // dohvati najnovije sa substringom
                }
                else if(upit.length()>9 && upit.substring(0,9).equals("korisnik:")){
                    String korisnik=upit.substring(9);
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    CustomResultReceiver mReceiver=new CustomResultReceiver(new Handler());
                    mReceiver.setReceiver(FragmentOnline.this);
                    intent.putExtra("receiver", mReceiver);
                    intent.putExtra("userID", korisnik);
                    getActivity().startService(intent);
                    // knjige poznanika sa substringom
                }
                else if(upit.isEmpty()){
                    return;
                }
                else{
                    // dohvati knjige sa jednom rijeci
                    split=false;
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(upit);

                }
            }
        });

        Button add = (Button)getView().findViewById(R.id.dAdd);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Knjiga k= knjige.get(spinerRez.getSelectedItemPosition());
                k.setKategorija(spinerKat.getSelectedItem().toString());
                k.setSlika1("");
                if(!KategorijeAkt.imaLiKnjige(k.getId())) {
                    long error = KategorijeAkt.openHelper.dodajKnjigu(k);
                    if (error == -1) {
                        Toast.makeText(getActivity(), "Greska!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    KategorijeAkt.upisaneKnjige.add(k);


                    for (Autor a : k.getAutori()
                            ) {

                        if (!KategorijeAkt.imaLiAutora(a.getImeiPrezime())) {
                            long error2 = KategorijeAkt.openHelper.dodajAutora(a.getImeiPrezime());
                            if (error2 ==-1){
                                Toast.makeText(getActivity(), "Greska!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            KategorijeAkt.autoriString.add(a.getImeiPrezime());
                        }
                            long error1 = KategorijeAkt.openHelper.dodajAutorstvo(a.getImeiPrezime(), k.getId());
                            if (error1 == -1 ) {
                                Toast.makeText(getActivity(), "Greska!", Toast.LENGTH_SHORT).show();
                                return;
                            }



                    }
                }

            }
        });

        Button back = (Button)getView().findViewById(R.id.dPovratak);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FrameLayout f1=(FrameLayout)getActivity().findViewById(R.id.mjesto1);
                Button dAutori =(Button)getActivity().findViewById((R.id.dAutori));
                Button dKategorije =(Button)getActivity().findViewById((R.id.dKategorije));
                Button dOnline =(Button)getActivity().findViewById((R.id.dDodajOnline));
                if(f1!=null) f1.setVisibility(View.VISIBLE);
                if(dAutori!=null) dAutori.setVisibility(View.VISIBLE);
                if(dKategorije!=null) dKategorije.setVisibility(View.VISIBLE);
                if(dOnline!=null)  dOnline.setVisibility(View.VISIBLE);
                FrameLayout online = (FrameLayout)getView().findViewById(R.id.mjestoOnline);
                if(online!=null)  online.setVisibility(View.INVISIBLE);
                getFragmentManager().popBackStack();
            }
        });

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        FrameLayout f1=(FrameLayout)getActivity().findViewById(R.id.mjesto1);
                        Button dAutori =(Button)getActivity().findViewById((R.id.dAutori));
                        Button dKategorije =(Button)getActivity().findViewById((R.id.dKategorije));
                        Button dOnline =(Button)getActivity().findViewById((R.id.dDodajOnline));
                        if(f1!=null) f1.setVisibility(View.VISIBLE);
                        if(dAutori!=null) dAutori.setVisibility(View.VISIBLE);
                        if(dKategorije!=null) dKategorije.setVisibility(View.VISIBLE);
                        if(dOnline!=null)  dOnline.setVisibility(View.VISIBLE);
                        FrameLayout online = (FrameLayout)getView().findViewById(R.id.mjestoOnline);
                        if(online!=null)  online.setVisibility(View.INVISIBLE);
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onNajnovijeDone (ArrayList<Knjiga> books){
        //knjige=books;
        knjige.clear();
        for (Knjiga k: books
                ) {
            knjige.add(k);
        }
        if(knjige.isEmpty()){
            Toast.makeText(getContext(), "Nije nadjena ni jedna knjiga!", Toast.LENGTH_LONG).show();
        }
        Spinner spinerRez=(Spinner)getView().findViewById(R.id.sRezultat);
        KnjigaSpinerAdapter adapter= new KnjigaSpinerAdapter(getContext(), knjige);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerRez.setAdapter(adapter);
    }

    @Override
    public void onDohvatiDone (ArrayList<Knjiga> books){
        //knjige=books;
        if(!split) knjige.clear();
        for (Knjiga k: books
             ) {
            knjige.add(k);
        }
        if(knjige.isEmpty()){
            Toast.makeText(getContext(), "Nije nadjena ni jedna knjiga!", Toast.LENGTH_LONG).show();
        }
        Spinner spinerRez=(Spinner)getView().findViewById(R.id.sRezultat);
        KnjigaSpinerAdapter adapter= new KnjigaSpinerAdapter(getContext(), knjige);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerRez.setAdapter(adapter);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData){
        switch (resultCode){
            case KnjigePoznanika.STATUS_START:
                //KOD
                break;
            case KnjigePoznanika.STATUS_FINISH:
                knjige=resultData.getParcelableArrayList("result");
                Spinner spinerRez=(Spinner)getView().findViewById(R.id.sRezultat);
                KnjigaSpinerAdapter adapter= new KnjigaSpinerAdapter(getContext(), knjige);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinerRez.setAdapter(adapter);
                if(knjige.isEmpty()){
                    Toast.makeText(getContext(), "Nije nadjena ni jedna knjiga!", Toast.LENGTH_LONG).show();
                }
                break;
            case KnjigePoznanika.STATUS_ERROR:
                Toast.makeText(getContext(), "Greska", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
