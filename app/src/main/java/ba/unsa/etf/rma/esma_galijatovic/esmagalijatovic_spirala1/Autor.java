package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by USAID on 06-Apr-18.
 */

public class Autor implements Parcelable{
    private String imeiPrezime;
    private int brojKnjiga;
    private ArrayList<String> knjige;

    public Autor(String imeIprezime, int brojKnjiga) {
        this.imeiPrezime = imeIprezime;
        this.brojKnjiga = brojKnjiga;
    }

    public Autor(String imeiPrezime, String idKnjige){
        this.imeiPrezime=imeiPrezime;
        this.knjige= new ArrayList<String>();
        knjige.add(idKnjige);
    }

    void dodajKnjigu(String id){
        for (String s: knjige
             ) {
            if(s.equals(id)) return;
        }
        knjige.add(id);
    }

    public Autor(){}

    protected Autor(Parcel p){
        imeiPrezime=p.readString();
        brojKnjiga=p.readInt();
    }
    public static final Creator<Autor> CREATOR=new Creator<Autor>() {
        @Override
        public Autor createFromParcel(Parcel parcel) {
            return new Autor(parcel);
        }

        @Override
        public Autor[] newArray(int i) {
            return new Autor[i];
        }
    };
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(imeiPrezime);
        dest.writeInt(brojKnjiga);
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeIprezime) {
        this.imeiPrezime = imeIprezime;
    }

    public int getBrojKnjiga() {
        return knjige.size();
    }

    public void setBrojKnjiga(int brojKnjiga) {
        this.brojKnjiga = brojKnjiga;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }
}
