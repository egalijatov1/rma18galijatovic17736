package ba.unsa.etf.rma.esma_galijatovic.esmagalijatovic_spirala1;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by USAID on 27-May-18.
 */

public class KategorijaCursorAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;
    private Context context;
    public KategorijaCursorAdapter(Context context, int layout, Cursor c, int flags){
        super(context, c, flags);
        this.context=context;
        mLayoutInflater=LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = mLayoutInflater.inflate(R.layout.lista_autor, parent, false);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor){
        TextView tv = (TextView)view.findViewById(R.id.eNaziv);
        tv.setText(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KAT_NAZIV)));
    }

}
